# Dockerfile et image

- Jusqu'ici nous avons utilisé des images toutes prêtes.

- Une des fonctionnalités principales de Docker est de pouvoir facilement construire des images à partie d'un simple fichier texte: le `Dockerfile`.

- On construit donc les images à partir d'un fichier `Dockerfile` en décrivant procéduralement (étape par étape) la construction.

Exemple:

```dockerfile
# Utilisation d'une image de base
FROM alpine:latest

# Commande qui sera exécutée lorsque le conteneur démarrera
CMD ["echo", "Bonjour, Docker !"]
```

- À savoir que chaque instructions du Dockerfile représente un layer, qui ne peut plus être modifier, ce système de fichiers fait du union mount.

- La commande pour construire l'image est : 

```bash
docker build [-t tag] [-f dockerfile] <build_context>
```

- généralement pour construire l'image, on se place directement dans le dossier avec le `Dockerfile` et les éléments de contexte nécessaire (programme, config, etc), le contexte est donc le caractère `.`, il est obligatoire de préciser un contexte.

- exmple : `docker build -t monalpine .`

- Le dockerfile est un fichier procédural qui permet de décrire l'installation d'un logiciel (la configuration d'un conteneur) en enchaînant des instructions Dockerfile (MAJUSCULE).

- Exemple:

```Dockerfile
# Image de base
FROM alpine:3.5

# Installe python and pip
RUN apk add --update py2-pip

# upgrade pip
RUN pip install --upgrade pip

# installe les modules python pour les besoin de l'application
COPY requirements.txt /usr/src/app/
RUN pip install --no-cache-dir -r /usr/src/app/requirements.txt

# Copier les fichiers nécessaires au bon fonctionnement de l'application
COPY app.py /usr/src/app/
COPY templates/index.html /usr/src/app/templates/

# Exposer le port sur lequel écouté notre application
EXPOSE 5000

# Lancer l'application
CMD ["python", "/usr/src/app/app.py"]
```

## Instruction

- FROM : L'image de base à partir de laquelle est construite l'image actuelle.

```Dockerfile
FROM alpine:3.5
```

- RUN : Permet de lancer une commande shell (installation, configuration)

```Dockerfile
RUN apk add --update py2-pip
```

- CMD : Généralement mis à la fin du Dockerfile, elle permet de préciser la commande qui va être lancé au lancement du conteneur avec docker run. on utilise une liste de paramètres

```Dockerfile
CMD ["--host", "0.0.0.0", "--port", "5000"]
```

- ENTRYPOINT : Précise le programme de base avec lequel sera lancé la commande.

```Dockerfile
ENTRYPOINT ["python", "app.py"]
```

- ENV : elle permet de définir des variables d'environnement qui seront disponibles lors de l'exécution des conteneurs docker.

```Dockerfile
ENV DATABASE_URL http://localhost:5432
```

- EXPOSE est utilisé pour informer Docker que le conteneur écoute sur des ports spécifiques au moment de son exécution.

```Dockerfile
EXPOSE 5000
```

- COPY est utilisé pour copier des fichiers ou des répertoires depuis le système de fichier local (hôte) vers le système de fichiers du conteneur Docker.

```Dockerfile
COPY <src (hôte)> <dest (conteneur)>
```
## Lancer la construction

- la commande pour lancer la construction d'une image est : 

```bash
docker build [-t <tag:version>] [-f <chemin_du_dockerfile>] <contexte_de_construction>
```
- Lors de la construction, Docker télécharge l'image de base (Declarer avec FROM).

- Il lance ensuite la séquence des instructions du Dockerfile.

# découverte d'une application web Node

- Récupérez d'abord une application node en la clonant:

```bash
git clone https://gitlab.com/cours-docker1/node-front-starter.git
```

- Ouvrez votre editeur de code, et lancer le terminal intégré en bas de l'écran


# Conteneuriser l'application web Node

- Dans le dossier du projet ajoutez un fichier nommé `Dockerfile` et sauvegardez-le.

- Avec les instructions plus au dessus et internet, essayez de build l'image node et déployer pour voir si elle est fonctionnelle.

je souhaite mettre quelques contraintes : 

- L'image du dockerfile doit être basé sur une `node:22.3`

- Le port de l'application doit être `EXPOSE` sur le port `3000`.

- Le `nom` du container doit se nommer `node-front-starter`.

des commandes comme ci-dessous vous seront utiles :

```bash
docker build -t node-front-starter .

docker run -d -p 3000:3000 node-front-starter
```

Une fois l'application déployé et fonctionnelle, attendre la suite du cours afin de prolonger l'exercice :)
