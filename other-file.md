# Optimisation de notre image

## Une image légère

- remplacez `node:22.3` par `node:22-alpine3.19`, puis comparez la taille de l'image à l'aide de `docker image`

## l'importance des layers

Pourquoi l'ordre des couches est-il important ?

 - Le cache des couches permet de réutliser les étapes déjà construites si elles n'ont pas changé.

 - Un ordre optimal minimise les reconstructions et rend le processus de construction plus rapide

Exemple : 

```dockerfile
FROM node:22

WORKDIR /usr/src/app

COPY . .

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]
```

Problèmes :

 - Chaque fois que le code source change, la couche 3 est invalidée, ce qui force la reconstruction de la couche 4

Exemple 2 :

```Dockerfile
FROM node:22-alpine

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
```

Couche 3 :

 - Les dépendances sont installés dans une couche distincte, qui ne sera reconstruite que si `package.json` ou `package-lock.json` changent

Couche 4 : 

 - Cette couche sera mise en cache et réutilisé tant que les fichiers de dépendances n'ont pas changé.

Couche 5 : 

 - Cette couche est invalidée et reconstruite seulement si le code sourche change, ce qui est fréquent, mais n'affecte pas les couches précédentes.

En finalité, on a améliorer les performances de construction de notre image, cela peut sembler infime, mais des images vous allez en faire des dizaines de milliers, ce qui est gain considérable sur le long termes.

## Autre bonnes pratiques

Minimiser les couches : 

 - Grouper plusieurs commandes `RUN` en une seule pour réduire le nombre de couches.

Nettoyer les dépendances temporaires : 

 - Supprimer les fichiers temporarires et les caches après l'installation des dépendances pour réduire la taille de l'image.

